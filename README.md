# BRIKKS Client

> Simple API client for accessing BRIKKS


**WORK IN PROGRESS**


## Subscriptions event client

> BRIKKS API Client for managing subscription events

```javascript
const 
    BrikksServices = require('brikks-client'),
    url = "https://api.brikks.com/v0",
    token = "secret-and-salted-api-token";

var client = BrikksServices.createSubscriptionsEventClient(url, token);
```

### Retrieve events

```javascript
client.pendingEvents((err, events) => {
    if ( err ) {
        console.log("Bad: ", err);
    } else {
        events.forEach(e => {
            console.log("Event:", e);
        });
    }
});
```
This will return an array of zero or more pending events. The event items are resources defined by the BRIKKS API.
Their current incarnation (representation) in JSON form is:

```json
{
  "id": 1234,
  "subscriptionId": 4321,
  "eventType": "activate-base-tv-product"
}
```

### Normalized/Extended/Decorated Subscription Event
 
> What to call it

```json
{
  "type": "subscription-event",
  "event-data": {
    "name": "activate-base-tv-product",
    "parameters": {
      "customerId": 12345,
      "customerName": "Nomen Nescius"      
    }  
  }
}
```

#### Event Types
> As defined by BRIKKS

* **activate-base-tv-product** - **inactivate**. Activating a base TV subscription and cancelling it
    * ``cardId``
    * ``productId``
    * ``hardwareId``

What's an **Event**:

The current incarnation of an event, as used in the Ljungby case, is:

```json
{
  "type":"subscription-event",
  "message":{
    "type":"activate-base-tv-product",
    "event": {
      "id": 1000,
      "subscriptionId": 1001,
      "eventType": "activate-base-tv-product"
    },    
    "parameters":{
      "customerId":10063,
      "customerName":"Rör inte Tvtest",
      "cardId":"12345678901234",
      "extraCardId":null,
      "hardwareId":"12121255",
      "productId":"314"
    }
  }
}
```

How can the event be made more general, with the purpose of being used for other integration purposes?

```json
{
  "id": "uuid-v4-string?",
  "type": "subscription-event",
  "event-data": {
    "name": "activate-base-tv-product",
    "parameters": {
      "customerId": "12345",
      "customerName": "Nomen Nescius"
      "cardId": "",
      ...
    }
    "links": [
      {
        "rel": "subscriptionevent",
        "href": "https://api.brikks.com/v0/subscriptionevents/{id}"
      },
      {
        "rel": "customer",
        "href": "https://api.brikks.com/v0/customers/12345"
      }
    ]
  }
}
```