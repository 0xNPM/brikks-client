/**
 * Test the...
 *
 * @module test-brikks-common
 * @author H Svalin (PE)
 */
"use strict";

const
    assert = require('chai').assert,
    CommonClient = require('../lib/brikks-common');

describe("Exercise some common features", function () {
    this.timeout(5000);

    let opts = require('./config.json').brikks;

    let client = Object.create(CommonClient);
    client.init(opts);

    it("should have options", function () {
        assert.isObject(client._options);
        assert(opts !== client._options);
        assert.deepEqual(opts, client._options);
    });

    it("should suceed in pinging", function(done) {
        client.ping((err, result) => {
            if ( err ) {
                done(err);
            } else {
                assert(result, "Ping failed!!");
                done();
            }
        })
    });
});    