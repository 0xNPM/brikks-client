/**
 *
 * @module test-subscriptions-client
 * @author H Svalin (PE)
 */
"use strict";

const
    assert = require('chai').assert;

const
    BrikksServices = require('../index');


describe("Basic SubscriptionClient object excercises", function() {

    let opts = require('./config.json').brikks;

    let client = BrikksServices.createSubscriptionsEventClient(opts.baseUrl, opts.token);

    it("should accept the options provided by init", function(){
        assert.isObject(client._options);
        assert( client._options.baseUrl === opts.baseUrl );
        assert( client._options.token === opts.token );
    });

    it("should get pending events", function(done) {
        client.pendingEvents((err, events)=>{
            if ( err ) {
                done(err);
            } else {
                assert.isArray(events);
                done();
            }
        });
    });

    it("should be possible to get resources, for example a customer", function(done){
        client.getResource(14716, "customers", (err, resource)=>{
            if ( err ) {
                done(err);
            } else {
                assert.isObject(resource);
                done();
            }
        });
    });

    it("should be possible to get a normalized event", function(done){
        this.timeout(5000);

        let event = {
            id: 1000,
            subscriptionId: 16940, //14986, //16940
            eventType: "activate-addon-tv-product"
        };

        client.getNormalizedEvent(event, (err, nEvent) => {
            if ( err ) {
                done(err);
            } else {
                assert.isObject(nEvent);
                done();
            }
        });

    });

});