/**
 * Test the...
 *
 * @module test-customers-client
 * @author H Svalin (PE)
 */
"use strict";

const
    assert = require('chai').assert,
    createCustomersClient = require('../index').createCustomersClient;


describe("Take the customers client through some basic tests", function () {
    this.timeout(5000);

    let opts = require('./config.json').brikks;
    let client = createCustomersClient(opts.baseUrl, opts.token);

    it("should ping", function (done) {

        client.ping((err, result)=>{
            if ( err ) {
                done(err);
            } else {
                assert(result, "Booh, no ping here");
                done();
            }
        });
    });

    it("should get a customer", function(done) {
        client.getCustomer(10063, (err, result)=>{
            if ( err ) {
                done(err);
            } else {
                assert.isObject(result);
                done();
            }
        });
    });
});    