# Tests

## Configuration

The tests expects a ``config.json`` file present in this folder, containing configurations
needed to run the tests. The file should contain the following:

```json
{
  "brikks": {
    "baseUrl": "https://api.brikks.com/v0",
    "token": "secret-token"
  }
}
```

> TBD explain the properties in the config