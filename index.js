/**
 * File:
 *
 *     index.js
 *
 * Description:
 *
 *      Defines a factory for various BRIKKS API clients.
 *
 * Node, JS Requirements:
 *
 *      ES6
 *
 * Created by:
 *
 *      hakansvalin on 2017-03-23.
 */
"use strict";

const VERSION_STRING = "2.0.0";

const
    SubscriptionsEventClient = require('./lib/brikks-subscriptions-client'),
    CustomersClient = require('./lib/brikks-customers-client');

var BrikksServices = {

    /**
     * Create a new SubscriptionEventsClient.
     *
     * @param {String} url - Base URL for BRIKKS API
     * @param {String} token - API access token
     */
    createSubscriptionsEventClient(url, token) {
        let result = Object.create(SubscriptionsEventClient);
        result.init({baseUrl: url, token: token});

        return result;
    },

    /**
     * Create a new CustomersClient.
     *
     * @param {String} url - Base URL for BRIKKS API
     * @param {String} token - API access token
     * @return {CustomersClient}
     */
    createCustomersClient(url, token) {
        let result = Object.create(CustomersClient);
        result.init({baseUrl: url, token: token});

        return result;
    }
};

module.exports = BrikksServices;