/**
 *
 * @module brikks-customers-client
 * @author H Svalin (PE)
 */
"use strict";

const
    CommonClient = require('./brikks-common');

const
    VERSION_STRING = "2.0.0";

/**
 * Inherit/Reuse from CommonClient
 *
 * @type {{Object}}
 */
var CustomersClient = Object.create(CommonClient);

/**
 * Get a customer.
 *
 * @param {String|Number} customerId
 * @param {CommonClient~resourceCallback} callback
 */
CustomersClient.getCustomer = function getCustomer(customerId, callback) {
    this.getResource(customerId, 'customers', callback);
};


module.exports = CustomersClient;

// Require main module, i.e. we are running this script standalone
if ( require.main === module ) {

}