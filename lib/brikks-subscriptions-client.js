/**
 *
 * @module brikks-subscriptions-client
 * @author H Svalin (PE)
 */
"use strict";

const VERSION_STRING = "2.0.0";

const
    CommonClient = require('./brikks-common'),

    _ = require('lodash'),
    ASQ = require('asynquence-contrib'),
    isOkResponse = require('./utils').isOkResponse,
    createErrorOnUnexpectedStatus = require('./utils').createErrorOnUnexpectedStatus;

const
    PATHS = [
        ['subscriptionevents', 'subscriptionevents'],
        ['customers', 'customers'],
        ['products', 'products'],
        ['subscriptions', 'subscriptions']
    ];

const EVENT_TYPE_PROPERTIES = [
        [
            ['activate-base-tv-product', 'inactivated'],
            [
                ['subscription.parameters.card_id', 'cardId'],
                ['subscriptions.parameters.hardware_id', 'hardwareId'],
                ['product.parameters.cryptoguard_id', 'productId']
            ]
        ],
        [
            ['blocked', 'unblocked'],
            [
                ['subscription.parameters.card_id', 'cardId']
            ]
        ],
        [
            ['activate-extra-tv-card', 'inactivate-extra-tvcard'],
            [
                ['subscription.parameters.card_id', 'extraCardId'],
                ['parentSubscription.parameters.card_id', 'cardId'],
                ['subscriptions.parameters.hardware_id', 'hardwareId']
            ]
        ],
        [
            ['activate-addon-tv-product', 'inactivate-addon-tv-product'],
            [
                function updateParamsForActivateAddonTVProduct(source, target) {

                    let sub = source.subscription;
                    if ( source.parentSubscription ) {
                        sub = source.parentSubscription;
                    }
                    target['productId'] = source.product.parameters.cryptoguard_id;
                    target['cardId'] = sub.parameters.card_id;
                    //target['hardwareId'] = sub.parameters.hardware_id;
                }
            ]
        ],
        [
            ['update-card'],
            [
                'subscription.parameters.card_id',
                'subscription.parameters.new_card_id'
            ]
        ],
        [
            ['update-hardware'],
            [
                'subscription.parameters.hardware_id',
                'subscription.parameters.new_hardware_id'
            ]
        ],
        [
            ['update-card-and-hardware'],
            [
                'subscription.parameters.card_id',
                'subscription.parameters.new_card_id',
                'subscription.parameters.hardware_id',
                'subscription.parameters.new_hardware_id'
            ]
        ]
    ];

/**
 * Create a normalized event based on the specified subscription event related
 * data.
 *
 * @param {Object} event - The event as returned from BRIKKS API
 * @param {Object} subscription - The subscription object as returned from BRIKKS API
 * @param {Object} parentSubscription - The parent subscription
 * @param {Object} product - The product as returned from BRIKKS API
 * @param {Object} customer - The customer object as returned from BRIKKS API
 */
function createNormalizedEvent(event, subscription, parentSubscription, product, customer) {

    function getPropertyPaths(eventType, propPaths) {
        propPaths = propPaths || EVENT_TYPE_PROPERTIES;

        let result = propPaths.find((e) => {
            return (e[0].includes(eventType) );
        });
        return result;
    }

    let result = {
        type: "subscription-event",
        "event-data": {
            id: event.id,
            type: event.eventType,
            parameters: {
                customerId: customer.id,
                customerName: customer.firstName + " " + customer.lastName
            }
        }
    };

    let source = {event, subscription, parentSubscription, product, customer},
        propertyPaths = getPropertyPaths(event.eventType) || [];

    if ( typeof propertyPaths[1][0] === 'function' ) {
        propertyPaths[1][0](source, result['event-data'].parameters);
    } else {
        propertyPaths.forEach((e) => {
            let sourceProperty = e[0],
                targetProperty = e[1];

            result['event-data'].parameters[targetProperty] = _.get(sourceProperty, e, null);
        });
    }
    return result;
}

/**
 * @typedef {Object} NormalizedEvent
 *
 * @property {String} type - Event type e.g. subscription-event
 * @property {String} created-at - Date and time when this was created
 * @property {Object} event-data - The event data
 * @property {String} event-data.type - The event type as defined by the SubscriptionEvent
 * @property {Number} event-data.id - The event id as defined by the SubscriptionEvent
 * @property {Object} event-data.parameters - The parameters needed to act on the event, these might
 *  vary.
 * @property {Number} event-data.parameters.customerId - The customers id
 * @property {String} event-data.parameters.customerName - The customers name
 */

/**
 * @typedef {Object} SubscriptionsEventClient~SubscriptionEvent
 *
 * @property {Number} id - The subscription event id
 * @property {Number} subscriptionId - The id of the concerned subscription
 * @property {String} eventType - Name/Moniker identifying the type of subscription event
 */


var SubscriptionsEventClient = Object.create(CommonClient);

/**
 *
 * @param {SubscriptionsEventClient~SubscriptionEvent} event - The event
 * @param {CommonBrikksClient~resourceCallback} callback
 */
SubscriptionsEventClient.getNormalizedEvent = function getNormalizedEvent(event, callback) {

    this._logger.debug("--> SubscriptionsEventClient::getNormalizedEvent");

    let self = this;

    ASQ(
        function(done) {
            self.getResource(event.subscriptionId, 'subscriptions', (err, sub)=>{
                if ( err ) {
                    done.fail(err);
                } else {
                    done(sub);
                }
            })
        }
    ).then(function(done, sub) {
        if ( 'parentSubscriptionId' in sub ) {
            self.getResource(sub.parentSubscriptionId, 'subscriptions', (err, parentsub) => {
                if ( err ) {
                    done.fail(err);
                } else {
                    done(sub, parentsub);
                }
            });
        } else {
            done(sub, null);
        }
    }).then(function(done, sub, parent){
        self.getResource(sub.productId, 'products', (err, product) => {
            if ( err ) {
                done.fail(err);
            } else {
                done(sub, parent, product);
            }
        });
    }).then(function(done, sub, parent, product) {
        self.getResource(sub.customerId, 'customers', (err, customer) => {
            if ( err ) {
                done.fail(err);
            } else {
                done(sub, parent, product, customer);
            }
        });
    }).val(function(subscription, parentSubscription, product, customer) {
        callback(null, createNormalizedEvent(event, subscription, parentSubscription, product, customer));
    }).or(function(err) {
        callback(err);
    });
};

/**
 * Get pending subscription events from BRIKKS.
 *
 * @param {SusbscriptionsEventClient~resourceCallback} callback - The callback called when events
 *  retrieved.
 */
SubscriptionsEventClient.pendingEvents = function pendingEvents(callback) {

    this._logger.debug("--> SubscriptionsEventClient::pendingEvents");

    this.request('subscriptionevents', 'GET', (err, response, body) => {
        if ( err ) {
            return callback(err);
        } else {
            if ( response.statusCode === 200) {
                callback(null, body.results);
            } else {
                callback(createErrorOnUnexpectedStatus(response));
            }
        }
    });
};

/**
 * Mark a subscription event as consumed, i.e. perform a delete on the subscription event
 * resource.
 *
 * @param {Number|String} subscriptionEventId - Subscription event id of the event to mark
 *  as being consumed.
 *
 * @param callback
 */
SubscriptionsEventClient.consumeEvent = function consumeEvent(subscriptionEventId, callback) {

    this._logger.debug("--> SubscriptionsEventClient::consumeEvent");

    const path = `subscriptionevents/${resourceId}`;
    this.request(path, "DELETE", (err, response, body) => {
        if ( err ) {
            return callback(err);
        }

        if ( isOkResponse(response) ) {
            callback(null, true);
        } else {
            callback(createErrorOnUnexpectedStatus(response));
        }
    });
};

/**
 * Update/Transit a subscriptions state,
 *
 * @example
 *
 *  let id = 123, stateName = "base-tv-product-activated",
 *      params = {
 *          pin_code: "8888"
 *      };
 *  client.transitionEvent(id, stateName, params, (err, response) => {
 *      if ( err ) {
 *          // Wen't completely haywire!
 *      } else {
 *          // Might have worked... inspect the response
 *      }
 *  });
 *
 * @param {Number|String} subscriptionId - The subscriptions id
 * @param {String} stateName - New/Next state name
 * @param {Object} params - Parameters
 * @param callback
 */
SubscriptionsEventClient.transitionEvent = function transitionEvent(subscriptionId, transitionName, parameters, callback) {

    this._logger.debug("--> SubscriptionsEventClient::transitionEvent");

    const path = `subscriptions/${subscriptionId}/transitions`;
    let body = {
        transitionName: transitionName,
        parameters: parameters
    };

    this.request(path, 'POST', body, (err, resp, body) => {
        if ( err )  {
            callback(err);
        } else {
            if ( isOkResponse(response) ) {
                callback(null, true);
            } else {
                callback(createErrorOnUnexpectedStatus(response));
            }
        }
    });
};

module.exports = SubscriptionsEventClient;

// If run standalone
if ( require.main === module ) {

}