/**
 *
 * @module brikks-common
 * @author H Svalin (PE)
 */
"use strict";

// Dependencies, huh
const
    request = require('request'),
    createDefaultLogger = require('./utils').createDefaultLogger,
    createErrorOnUnexpectedStatus = require('./utils').createErrorOnUnexpectedStatus;

// Constant values
const
    VERSION_STRING = "2.0.0",
    DEFAULT_REQUEST_OPTIONS = {
        headers: {
            "User-Agent": "BRIKKS-Subscriptions-Client V_" + VERSION_STRING,
            "Content-Type": "application/json"
        }
    },
    DEFAULT_OPTIONS = {
        baseUrl: null,
        token: null
    };

/**
 * BRIKKS API Client commonalities.
 *
 * @type {{}}
 */
var CommonBrikksClient = {

    /**
     * Initialize this Commons object.
     *
     * @param {Object} options - Configuration object
     */
    init(options, logger) {
        this._options = Object.assign({}, DEFAULT_OPTIONS, options);

        if ( !logger ) {
            this._logger = createDefaultLogger();
        } else {
            this._logger = logger;
        }
        return this;
    },

    /**
     * Request helper function. calls <code>request</code> with the
     * specified parameters and some extra.
     *
     * @param {String} path - Resource path to request, e.g. mypath/resource/id
     * @param {String} method - Method to use, e.g. GET (default)
     * @param {Object|Callback} [body] - Optional body to include in the request
     * @param {requestCallback} callback - Callback
     *
     * @private
     */
    request(path, method, bodyOrCallback, callback) {

        if ( typeof bodyOrCallback === 'function' ) {
            callback = bodyOrCallback;
        }

        let opts = {
            url: '',
            method: method,
            headers: Object.assign({}, DEFAULT_REQUEST_OPTIONS.headers, {"Authorization": "Bearer " + this._options.token}),
            json: true
        };

        if ( typeof bodyOrCallback === 'object' ) {
            opts.body = bodyOrCallback;
        }

        opts.url = `${this._options.baseUrl}/${path}`;
        request(opts, callback);
    },

    /**
     * Retrieve an identified resource.
     *
     * @param {String|Number} resourceId - The resources id
     * @param {String} path - Resource path
     * @param {CommonBrikksClient~resourceCallback} callback - Callback called when resource is retrieved
     * @return {Object}
     */
    getResource(resourceId, path, callback) {
        path = `${path}/${resourceId}`;

        this._logger.debug("--> CommonBrikksClient::getResource path: " + path);

        this.request(path, "GET", (err, response, body) => {
            if ( err ) {
                return callback(err);
            } else {
                if ( response.statusCode === 200 ) {
                    callback(null, body);
                } else {
                    callback(createErrorOnUnexpectedStatus(response));
                }
            }
        })
    },

    /**
     * Performs a HEAD request to the BRIKKS API base URL.
     *
     * @example
     *
     *  client.ping((err, ok) => {
     *      if ( err ) {
     *          // Bad
     *      } else {
     *          if ( ok ) {
     *              // Nice!!
     *          } else {
     *              // Hmm, that's curious...
     *          }
     *      }
     *  });
     *
     * @param {} callback - Callback called with error object as the first argument
     *  and the outcome of the ping as the second argument. The outcome is represented
     *  with a boolean value, <code>true</code> is a success, <code>false</code> is not!
     */
    ping(callback) {
        this._logger.debug("--> CommonBrikksClient::ping");

        let opts = {
            url: `${this._options.baseUrl}`,
            method: 'HEAD',
            headers: {
                "User-Agent": DEFAULT_REQUEST_OPTIONS.headers["User-Agent"]
            }
        };

        request(opts, (err, response, body) => {
            if ( err ) {
                return callback(err);
            }

            if ( response.statusCode === 200 || response.statusCode === 301 ) {
                callback(null, true);
            } else {
                callback(null, false);
            }
        });
    }

    /**
     * @callback CommonBrikksClient~requestCallback
     *
     * @param {Error|null} err - Error object if any
     * @param {IncomingMessage} response - The response
     * @param {String|Object} body - The response body
     */

    /**
     * Callback called when retrieving one or more resources.
     *
     * @callback CommonBrikksClient~resourceCallback
     * @param {Error|null} err - Error if any
     * @param {Object|Object[]} [resource] - The retrieved resource(s)
     */
};

// Export the common client, please!
module.exports = CommonBrikksClient;

if ( require.main === module ) {
    // We're run standalone
    console.log('Running standalone!');
}