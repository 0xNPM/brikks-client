/**
 *
 * @module utils
 * @author H Svalin (PE)
 */
"use strict";

const
    winston = require('winston');

// Status codes regarded as OK by this BRIKKS Client
const
    VERSION_STRING = "2.0.0",
    OK_STATUS_CODES = [200, 201, 202, 204];

/**
 * Create a default Winston logger to use when no other is specified...
 *
 * @param {String} name - Name of the logger
 */
function create_default_logger(name, level) {
    name = name || 'Default Logger';
    level = level || 'debug';

    let result = new (winston.Logger)({
        name: name,
        level: level,
        transports: [
            new winston.transports.Console()
        ]
    });
    return result;
}


/**
 * Create an Error object due to an unexpected status code.
 *
 * @param {IncomingMessage} response - The incoming message from the <code>request</code> library.
 * @param {String} [message] - Optional error message
 * @return {Error}
 */
function createErrorOnUnexpectedStatus(response, message) {

    let defaultMessage = `Unexpected response code ${response.statusCode} when requesting ${response.request.path}`;
    message = message || defaultMessage;
    return new Error(message);
}

/**
 *
 * @param {Number[]} codes -
 * @param {IncomingMessage} response - A response from <code>request</code>
 * @return {Assertion|boolean}
 */
function responseStatusCodeIn(codes, response) {
    return codes.includes(response.statusCode);
}


module.exports = {
    createDefaultLogger: create_default_logger,
    createErrorOnUnexpectedStatus: createErrorOnUnexpectedStatus,
    isOkResponse: responseStatusCodeIn.bind(undefined, OK_STATUS_CODES)
};